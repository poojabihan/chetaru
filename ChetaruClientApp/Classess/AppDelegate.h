//
//  AppDelegate.h
//  ChetaruClientApp
//
//  Created by Alok Mishra on 10/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Reachability.h"
#import <UserNotifications/UserNotifications.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate,UINavigationControllerDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;
@property(nonatomic) BOOL  _isCurrentpageChat;
@property(nonatomic) BOOL  _isQuesFromNotify;
@property NSString * urlString;
@property (retain, nonatomic)  Reachability* reach;
@end

