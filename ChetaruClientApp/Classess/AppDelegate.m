//
//  AppDelegate.m
//  ChetaruClientApp
//
//  Created by Alok Mishra on 10/11/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import "AppDelegate.h"
#import "Reachability.h"
#import <UserNotifications/UserNotifications.h>
@import Firebase;
@import FirebaseMessaging;
#import "ChatViewController.h"
#import "JCNotificationCenter.h"
#import "JCNotificationBannerPresenterSmokeStyle.h"
#import <SCLAlertView.h>
#import "HomeViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    self.reach = [Reachability reachabilityForInternetConnection]; //retain reach
    [self.reach startNotifier];
    
    NetworkStatus remoteHostStatus = [self.reach currentReachabilityStatus];
    
    NSLog(@"???? ALWAYS INITS WITH Not Reachable ????");
    if(remoteHostStatus == NotReachable) {NSLog(@"init **** Not Reachable ****");}
    else if (remoteHostStatus == ReachableViaWiFi) {NSLog(@"int **** wifi ****"); }
    else if (remoteHostStatus == ReachableViaWWAN) {NSLog(@"init **** cell ****"); }
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(tokenRefreshCallBack:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    
    if( SYSTEM_VERSION_LESS_THAN( @"10.0" ) )
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        //if( option != nil )
        //{
        //    NSLog( @"registerForPushWithOptions:" );
        //}
    }
    else
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        // [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
        
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * error)
         {
             if (granted==NO) {
                 NSLog(@"Didnt allowed");
                 UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Notification Services Disabled!"  message:@"Please open this app's settings enable Notifications to get important Updates!"  preferredStyle:UIAlertControllerStyleAlert];
                 [alertController addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                             {
                                                 @try
                                                 {
                                                     NSLog(@"tapped Settings");
                                                     BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
                                                     if (canOpenSettings)
                                                     {
                                                         NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                         
                                                         
                                                         [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
                                                     }
                                                 }
                                                 @catch (NSException *exception)
                                                 {
                                                     
                                                 }
                                             }]];
                 [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
                 UINavigationController *nvc = (UINavigationController *)[[application windows] objectAtIndex:0].rootViewController;
                 UIViewController *vc = nvc.visibleViewController;
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [vc presentViewController:alertController animated:YES completion:nil];
                 });
                 
             }
             if(error)
             {
                 NSLog( @"Push registration FAILED" );
                 NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                 NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                      [[UIApplication sharedApplication] registerForRemoteNotifications];
                 });
                 // required to get the app to do anything at all about push notifications
                 NSLog( @"Push registration success." );
                 
             }
         }];
    }
    [FIRApp configure];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    return YES;
}

- (void) reachabilityChanged:(NSNotification *)notice
{
    
    NSLog(@"!!!!!!!!!! CODE IS CALL NOW !!!!!!!!!!");
    
    NetworkStatus remoteHostStatus = [self.reach currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) {NSLog(@"**** Not Reachable ****");}
    else if (remoteHostStatus == ReachableViaWiFi) {NSLog(@"**** wifi ****"); }
    else if (remoteHostStatus == ReachableViaWWAN) {NSLog(@"**** cell ****"); }
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    UILocalNotification *notification=[[UILocalNotification alloc]init];
    notification.applicationIconBadgeNumber=-1;
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    [[FIRMessaging messaging]disconnect];
    NSLog(@"Disconnected From FCM");
    [self resetBadgeCountInServer];
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    UILocalNotification *notification=[[UILocalNotification alloc]init];
    notification.applicationIconBadgeNumber=-1;
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    [self connectToFirebase];
    [self resetBadgeCountInServer];
}
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    //    BOOL isValid = [self pushNotificationsEnabled];
    //    if(isValid==false)
    //    {
    //        //your code
    //        NSLog(@"is valid %id ",isValid);
    //        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Notification Services Disabled!" message:@"In order to be notified, please open this app's settings enable Notifications to get important Updates!" preferredStyle:UIAlertControllerStyleAlert];
    //        UIAlertAction * action=[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
    //        [alert addAction:action];
    //        UINavigationController *nvc = (UINavigationController *)[[application windows] objectAtIndex:0].rootViewController;
    //        UIViewController *vc = nvc.visibleViewController;
    //        [vc presentViewController:alert animated:YES completion:nil];
    //
    //
    //    }
    NSLog(@"Failed to get token, error: %@", error.localizedDescription);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler {
    NSLog(@"Message Id: %@",userInfo[@"gcm.message_id"]);
    NSLog(@"Message %@",userInfo);
    NSLog(@"Notification Delivered");
    
    // iOS 10 will handle notifications through other methods
    
    if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO( @"10.0" ) )
    {
        NSLog( @"iOS version >= 10. Let NotificationCenter handle this one." );
        
        // set a member variable to tell the new delegate that this is background
        return;
    }
    NSLog( @"HANDLE PUSH, didReceiveRemoteNotification: %@", userInfo );
    
    // custom code to handle notification content
    
    if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive )
    {
        NSLog( @"INACTIVE" );
        
        //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
        if ([userInfo objectForKey:@"changeit_id"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self._isCurrentpageChat=YES;
                // UITabBarController *tab = (UITabBarController *)self.window.rootViewController;
                
                UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
                UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                
                ChatViewController * chatVC=(ChatViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"chat"];
                chatVC.itemID=[userInfo objectForKey:@"changeit_id"];
                
                //[(UINavigationController *)tab.selectedViewController pushViewController:chatVC animated:YES];
                [navigationController pushViewController:chatVC animated:NO];
                handler( UIBackgroundFetchResultNewData );
                
                
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                self._isQuesFromNotify=YES;
                
                
                //UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
                
                
                UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                
                // ViewController * homeVC=(ViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"vc"];
                // [(UINavigationController *)tabBarController.selectedViewController pushViewController:homeVC animated:YES];
                AppDelegate * app=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                UIViewController * firstVC=[storyBoard1 instantiateViewControllerWithIdentifier:@"vc"];
                app.window.rootViewController=firstVC;
                [app.window makeKeyAndVisible];
                
                
                
                //                ViewController *login = (ViewController*)[storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                //                UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:login];
                //                self.window.rootViewController=nav;
                
                
                handler( UIBackgroundFetchResultNewData );
            });
            
        }
        
    }
    else if( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground )
    {
        NSLog( @"BACKGROUND" );
        handler( UIBackgroundFetchResultNewData );
    }
    else
    {
        NSLog( @"FOREGROUND" );
        if ([userInfo objectForKey:@"changeit_id"]) {
            [self playNotificationSound];
            [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterSmokeStyle new];
            [JCNotificationCenter
             enqueueNotificationWithTitle:[[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"title"]
             message:[[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"body"]
             tapHandler:^{
                 
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     self._isCurrentpageChat=YES;
                     //  UITabBarController *tab = (UITabBarController *)self.window.rootViewController;
                     
                     UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
                     UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                     
                     ChatViewController * chatVC=(ChatViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"chat"];
                     chatVC.itemID=[userInfo objectForKey:@"changeit_id"];
                     //[(UINavigationController *)tab.selectedViewController pushViewController:chatVC animated:YES];
                     [navigationController pushViewController:chatVC animated:NO];
                     handler( UIBackgroundFetchResultNewData );
                 });
             }];
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
                [alert addButton:@"Show" actionBlock:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self._isQuesFromNotify=YES;
                        //  UITabBarController* tabBarController = (UITabBarController *)self.window.rootViewController;
                        
                        //UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
                        UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                        
                        
                        //  ViewController * homeVC=(ViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"vc"];
                        //                        ViewController *login = (ViewController*)[storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                        //                        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:login];
                        //                        self.window.rootViewController=nav;
                        AppDelegate * app=(AppDelegate *)[[UIApplication sharedApplication] delegate];
                        
                        UIViewController * firstVC=[storyBoard1 instantiateViewControllerWithIdentifier:@"vc"];
                        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:firstVC];
                        app.window.rootViewController=nav;
                        [app.window makeKeyAndVisible];
                        handler( UIBackgroundFetchResultNewData );
                    });
                }];
                [self playNotificationSound];
                
                [alert showCustom:[UIImage imageNamed:@"alertIcon.jpg"] color:khudColour title:kAppNameAlert subTitle:@"Give us your Feedback." closeButtonTitle:nil duration:0.0f];
            });
        }
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}
#pragma mark PushNotification methods below iOS 10
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
    NSLog(@"didregister");
    if ([[UIApplication sharedApplication] currentUserNotificationSettings].types != UIUserNotificationTypeNone)
    {
        NSLog(@" Push Notification ON");
    }
    else
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Notification Services Disabled!"  message:@"Please open this app's settings enable Notifications to get important Updates!"  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                    {
                                        @try
                                        {
                                            NSLog(@"tapped Settings");
                                            BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
                                            if (canOpenSettings)
                                            {
                                                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
                                            }
                                        }
                                        @catch (NSException *exception)
                                        {
                                            
                                        }
                                    }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        UINavigationController *nvc = (UINavigationController *)[[application windows] objectAtIndex:0].rootViewController;
        UIViewController *vc = nvc.visibleViewController;
        [vc presentViewController:alertController animated:YES completion:nil];
    }
    
    
}
-(void)playNotificationSound{
    
    //play sound
    SystemSoundID    pewPewSound;
    NSString *pewPewPath = [[NSBundle mainBundle]
                            pathForResource:@"sms_alert_aurora" ofType:@"caf"];
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pewPewURL, & pewPewSound);
    AudioServicesPlaySystemSound(pewPewSound);
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog( @"Handle push from foreground" );
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
    // custom code to handle push while app is in the foreground
    /*if (!(notification.request.content.userInfo==NULL)) {
     self._isCurrentpageChat=YES;
     UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
     UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
     
     ChatViewController * chatVC=(ChatViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"chat"];
     chatVC.itemID=[notification.request.content.userInfo objectForKey:@"changeit_id"];
     [navigationController pushViewController:chatVC animated:NO];
     }*/
    
    NSLog(@"%@", notification.request.content.userInfo);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler
{
    if ([response.notification.request.content.userInfo objectForKey:@"changeit_id"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self._isCurrentpageChat=YES;
            UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
            //  UITabBarController *tab = (UITabBarController *)self.window.rootViewController;
            
            
            UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            
            ChatViewController * chatVC=(ChatViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"chat"];
            chatVC.itemID=[response.notification.request.content.userInfo objectForKey:@"changeit_id"];
            // [(UINavigationController *)tab.selectedViewController pushViewController:chatVC animated:YES];
            
            //app.window.rootViewController=chatVC;
            [navigationController pushViewController:chatVC animated:NO];
            completionHandler();
        });
        
    }
    else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self._isQuesFromNotify=YES;
            
            // UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
            UIStoryboard *storyBoard1 = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            
            // ViewController * homeVC=(ViewController *)[storyBoard1 instantiateViewControllerWithIdentifier:@"vc"];
            //homeVC.quesID=[userInfo objectForKey:@"ques_id"];
            // [(UINavigationController *)tabBarController.selectedViewController pushViewController:homeVC animated:YES];
            
            AppDelegate * app=(AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            UIViewController * firstVC=[storyBoard1 instantiateViewControllerWithIdentifier:@"vc"];
            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:firstVC];
            app.window.rootViewController=nav;
            [app.window makeKeyAndVisible];
            
            
            //        ViewController *login = (ViewController*)[storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            //            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:login];
            //            self.window.rootViewController=nav;
            
            //[navigationController pushViewController:homeVC animated:NO];
            completionHandler();
            
        });
    }
    
    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSLog(@"%@", response.notification.request.content.userInfo);
}

-(void)resetBadgeCountInServer{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        // getting an NSString
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        if (emailIdString!=NULL) {
            
            
            NSLog(@"email id in Appdelgate %@",emailIdString);
            if (emailIdString==NULL) {
                self.urlString = [NSString stringWithFormat:@"badgecount?email_id=&app_name=%@",kAppNameAPI];
            }
            else{
                
                
                self.urlString = [NSString stringWithFormat:@"badgecount?email_id=%@&app_name=%@",emailIdString,kAppNameAPI];
            }
            
            NSURL *baseURL = [NSURL URLWithString:FBaseURL];
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
            [manager GET:self.urlString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSLog(@"Response :%@",responseObject);
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"Error :%@",error);
            }];
            
        }
        
    }
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(id)annotation{
    
    return YES;
    
}

#pragma mark Firebase methods

-(void)tokenRefreshCallBack:(NSNotification *)notification{
    NSString * refreshedToken=[[FIRInstanceID instanceID]token];
    NSLog(@"InstanceId Token %@",refreshedToken);
    //Connect to FCM
    [self connectToFirebase];
    
}

-(void)connectToFirebase{
    [[FIRMessaging messaging]connectWithCompletion:^(NSError * _Nullable error) {
        if (error!=nil) {
            NSLog(@"Unable to Connect to FCM %@",error.localizedDescription);
        }
        else{
            NSLog(@"Connected to FCM");
        }
    }];
}
- (void)tokenRefreshNotification:(NSNotification *)notification {
    NSLog(@"instanceId_notification=>%@",[notification object]);
    NSString * instanceID = [NSString stringWithFormat:@"%@",[notification object]];
    NSLog(@"String %@",instanceID);
    
    [self connectToFirebase];
}
- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"ChetaruClientApp"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
