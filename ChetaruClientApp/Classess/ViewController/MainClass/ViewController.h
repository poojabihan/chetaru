//
//  ViewController.h
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import "Webservice.h"
@interface ViewController : UIViewController<UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate,UIAlertViewDelegate,UIActionSheetDelegate, CLLocationManagerDelegate,MFMailComposeViewControllerDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UITextView *commentTxtView;

@property (weak, nonatomic) IBOutlet UIButton *rghtButton;

@property (weak, nonatomic) IBOutlet UITextField *locationText;

@property (weak, nonatomic) IBOutlet UIImageView *imageVw;

@property (weak, nonatomic) IBOutlet UITextField *textField1;

- (IBAction)takeAndAddPhoto:(id)sender;

- (IBAction)onSendingMail:(id)sender;

- (IBAction)sendLocation:(id)sender;

-(void)sendingMail;

@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

- (IBAction)deleteButtonClick:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *historyButton;
@property (weak, nonatomic) IBOutlet UIButton *messageButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewInVC;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *takeAndSendButtonInstance;

@property NSString * stringToCheckVC,*emailURL,*itemPostURL,* URLString,* refreshedToken;

@end

