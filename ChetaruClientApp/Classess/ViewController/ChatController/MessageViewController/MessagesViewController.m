//
//  MessagesViewController.m
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "MessagesViewController.h"
#import "MessageItemTableViewCell.h"
#import "ChatViewController.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import <MBProgressHUD.h>
#import "UIColor+Hexadecimal.h"


@interface MessagesViewController ()
{
    NSArray * dictArray,*itemChatArray,*resultArray;
    NSMutableDictionary *dictTVInbox;
    //UIRefreshControl *refreshControl;
    MessageItemTableViewCell * messageItemCell;
    MBProgressHUD * hud;
    BOOL flag;
    
}
@end
NSManagedObjectContext *messageContext, * chatContextMessage;
;

@implementation MessagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kReachabilityChangedNotification object:nil];
    flag=YES;
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [self.navigationItem setTitle:@"Messages"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    
    //AppDelegate *app=(AppDelegate*)[[UIApplication sharedApplication]delegate];
 
    
    self.messagesTableView.dataSource = self;
    self.messagesTableView.delegate = self;
    //    refreshControl = [[UIRefreshControl alloc] init];
    //    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    //    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    //
    //    [self.messagesTableView addSubview:refreshControl];
    //[self loadingElementsInMessage];
    
    
}
- (void)dealloc{
    //[super dealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification
{
    NSLog (@"Notification Data %@",notification.userInfo);
    if ([[notification name] isEqualToString:@"kNetworkReachabilityChangedNotification"])
    {
        NSLog (@"Successfully received the kNetworkReachabilityChangedNotification!");
        
        [self loadingElementsInInboxWithBackgroundThread];
        
        
        
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    //[self loadingElementsInMessage];
    if (flag==YES) {
        [self loadingElementsInMessage];
        flag=NO;
    }
    else{
        //[self performSelectorInBackground:@selector(loadingElementsInMessage) withObject:nil];
        [self loadingElementsInInboxWithBackgroundThread];
    }
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
    });
    
    
}

-(void)loadingElementsInMessage{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    //hud.contentColor =khudColour;
    hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        NSString *str;
        dictArray = [[NSArray alloc]init];
        itemChatArray= [[NSArray alloc]init];
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
       
        // getting an NSString
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        NSLog(@"email id in inbox %@",emailIdString);
        if (emailIdString==NULL) {
            str= [NSString stringWithFormat:@"getinboxitem?email_id=&app_name=%@",kAppNameAPI];
        }
        else{
            str = [NSString stringWithFormat:@"getinboxitem?email_id=%@&app_name=%@",emailIdString,kAppNameAPI];
        }
        
        NSLog(@"Link %@",str);
        
        NSURL *baseURL = [NSURL URLWithString:FBaseURL];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
        [manager GET:str parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"Response :%@",responseObject);
            if ([responseObject objectForKey:@"response"]) {
                dictArray = [responseObject objectForKey:@"response"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    [self.messagesTableView reloadData];
                });
                
                //[refreshControl endRefreshing];
                NSLog(@"Array %@",dictArray);
                NSLog(@"Count of Array %lu",(unsigned long)dictArray.count);
                NSArray * arr;
                BOOL foo = true;
                
                for (NSMutableDictionary *dictInFor in dictArray) {
                    arr=[dictInFor objectForKey:@"itemchat"];
                    //NSLog(@"count of item chat in all %lu",(unsigned long)arr.count);

                    
                    
                }
                NSLog(@"Inbox Wow %d ",foo);
                if (foo==false) {
                    
                    [dictArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        
                        [messageContext performBlockAndWait:^{
                                                       itemChatArray=[obj objectForKey:@"itemchat"];
                            //[self deleteAllObjectsInChat:@"Chat"];
                            
                            
                            
                            [itemChatArray enumerateObjectsUsingBlock:^(id  _Nonnull chatObj, NSUInteger idx, BOOL * _Nonnull stop) {
                                
                                if (chatContextMessage != nil) {
                                    [chatContextMessage performBlockAndWait:^{
                                        
                                    }];
                                }
                                
                                
                                
                            }];
                            
                            
                            
                            NSError *error;
                            
                            
                            if (![messageContext save:&error]) {
                                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                            }
                        }];
                    }];
                    
                }
            }
            
            else if (responseObject==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    // [refreshControl endRefreshing];
                });
                
                
              
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    // [refreshControl endRefreshing];
                });
                
               
                
                
                
            }
            
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"Error :%@",error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
                // [refreshControl endRefreshing];
            });
     
            
        }];
        
        
        
        
        
        
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
            //[refreshControl endRefreshing];
        });
        [messageContext performBlockAndWait:^{
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Messages" inManagedObjectContext:messageContext];
            [fetchRequest setEntity:entity];
            NSError *error;
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createddate" ascending:NO];
            
            [fetchRequest setSortDescriptors:@[sortDescriptor]];
            
            
            dictArray = [messageContext executeFetchRequest:fetchRequest error:&error];
        }];
        
    }
    
}

-(void)loadingElementsInInboxWithBackgroundThread{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    
    
    // Fire off an asynchronous task, giving UIKit the opportunity to redraw wit the HUD added to the
    // view hierarchy.
    //dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
    
    // Do something useful in the background
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        //[self.view setUserInteractionEnabled:NO];
        
        //            UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        //           [activity setColor:[UIColor grayColor]];
        //            activity.center = self.navigationController.view.center;
        //
        //
        //
        //
        //            [self.view addSubview:activity];
        //            [activity startAnimating];
        
        NSString *str;
        dictArray = [[NSArray alloc]init];
        itemChatArray= [[NSArray alloc]init];
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
       
        // getting an NSString
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        NSLog(@"email id in inbox %@",emailIdString);
        if (emailIdString==NULL) {
            str= [NSString stringWithFormat:@"getinboxitem?email_id=&app_name=%@",kAppNameAPI];
        }
        else{
            str = [NSString stringWithFormat:@"getinboxitem?email_id=%@&app_name=%@",emailIdString,kAppNameAPI];
        }
        
        
        NSURL *baseURL = [NSURL URLWithString:FBaseURL];
        dispatch_queue_t myQueue = dispatch_queue_create("com.Chetaru.TellDan.loadingElementsInInboxWithBackgroundThread", DISPATCH_QUEUE_SERIAL);
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        [manager setCompletionQueue:myQueue];
        
        
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
        [manager GET:str parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject objectForKey:@"response"]) {
                dictArray = [responseObject objectForKey:@"response"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.messagesTableView reloadData];
                    //[activity stopAnimating];
                    [self.view setUserInteractionEnabled:YES];
                    //Any UI updates should be made here .
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                    
                    
                    
                });
                
                
                NSArray * arr;
                BOOL foo = true;
                
                for (NSMutableDictionary *dictInFor in dictArray) {
                    arr=[dictInFor objectForKey:@"itemchat"];
                    //NSLog(@"count of item chat in all %lu",(unsigned long)arr.count);
                    
                    
                }
                NSLog(@"Inbox Wow %d ",foo);
                if (foo==false) {
                   
                    [dictArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        
                        [messageContext performBlockAndWait:^{
                            
                            
                            itemChatArray=[obj objectForKey:@"itemchat"];
                            //[self deleteAllObjectsInChat:@"Chat"];
                            
                            
                            
                            [itemChatArray enumerateObjectsUsingBlock:^(id  _Nonnull chatObj, NSUInteger idx, BOOL * _Nonnull stop) {
                                
                                if (chatContextMessage != nil) {
                                    [chatContextMessage performBlockAndWait:^{
                                        
                                        
                                    }];
                                }
                                
                                
                                
                            }];
                            
                            
                            
                            NSError *error;
                            
                            
                            if (![messageContext save:&error]) {
                                
                                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                });
                                
                            }
                        }];
                    }];
                    
                }
            }
            else if (responseObject==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[activity stopAnimating];
                    //[self.view setUserInteractionEnabled:YES];
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
             
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[activity stopAnimating];
                    //[self.view setUserInteractionEnabled:YES];
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
               
                
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error = %@", error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                // [self.view setUserInteractionEnabled:YES];
                //[activity stopAnimating];
            });
           
            
            
        }];
        
    }
    
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            //Any UI updates should be made here .
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        [messageContext performBlockAndWait:^{
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"Messages" inManagedObjectContext:messageContext];
            [fetchRequest setEntity:entity];
            NSError *error;
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createddate" ascending:NO];
            
            [fetchRequest setSortDescriptors:@[sortDescriptor]];
            
            
            dictArray = [messageContext executeFetchRequest:fetchRequest error:&error];
        }];
        
    }
    // IMPOR TANT - Dispatch back to the main thread. Always access UI
    // classes (including MBProgressHUD) on the main thread.
    
    
    
    
}

#pragma mark PullToRefresh Method


//-(void)refreshData{
//    [self loadingElementsInMessage];
//}

#pragma mark Fetching Coredata Methods



#pragma mark Gesture Method

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

#pragma mark TableViewDelegate Methods



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dictArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    messageItemCell = [tableView dequeueReusableCellWithIdentifier:@"messageCell" forIndexPath:indexPath];
    
    
    if (messageItemCell == nil) {
        messageItemCell = [[MessageItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"messageCell"];
    }
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        NSLog(@"Array Print %@",dictArray);
        dictTVInbox = [dictArray objectAtIndex:indexPath.row];
        
        
        
        messageItemCell.lastMessageLabel.text=[[[dictTVInbox objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"message"];
        
        messageItemCell.titleHeadLineLabel.text = [dictTVInbox objectForKey:@"msg_detail"];
        if ([[dictTVInbox objectForKey:@"msg_type"] isEqualToString:@"ann"]) {
            messageItemCell.timeLabel.text =[NSString stringWithFormat:@"Tell TheStationMaster : %@ ",[[[dictTVInbox objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"created_date"]];
            
        }else{
            messageItemCell.timeLabel.text =[NSString stringWithFormat:@"Tell TheStationMaster : %@  (Item %@)",[[[dictTVInbox objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"created_date"],[dictTVInbox objectForKey:@"changeit_id"]];
        }
    }
    else{
        
        
    }
    
    
    return messageItemCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //
    //    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    //    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    //    if(internetStatus != NotReachable)
    //    {
    //        NSMutableDictionary *rowDict = [dictArray objectAtIndex:indexPath.row];
    //        ChatViewController *chatController = [self.storyboard instantiateViewControllerWithIdentifier:@"chat"];
    //        chatController.itemID=[rowDict objectForKey:@"changeit_id"];
    //        NSLog(@"chang id in inbox %@",[rowDict objectForKey:@"changeit_id"]);
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //           [self.navigationController pushViewController:chatController animated:YES];
    //        });
    //
    //    }
    //    else
    //    {
    //        Messages *info = [dictArray objectAtIndex:indexPath.row];
    //        ChatViewController *chatController = [self.storyboard instantiateViewControllerWithIdentifier:@"chat"];
    //        chatController.itemID=info.changeitid;
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            [self.navigationController pushViewController:chatController animated:YES];
    //        });
    //
    //    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 103;
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"chatviewSegueFromMVC"]) {
        NSIndexPath * indexPath = [self.messagesTableView indexPathForSelectedRow];
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if(internetStatus != NotReachable)
        {
            NSMutableDictionary *rowDict = [dictArray objectAtIndex:indexPath.row];
            ChatViewController * chatController=[segue destinationViewController];
            chatController.itemID=[rowDict objectForKey:@"changeit_id"];
            NSLog(@"chang id in inbox %@",[rowDict objectForKey:@"changeit_id"]);
            
            
        }
        
        
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
