//
//  HistoryTableViewCell.h
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imagevw;

@property (weak, nonatomic) IBOutlet UILabel *textOfLocation;

@property (weak, nonatomic) IBOutlet UILabel *dateAndTime;

@property (weak, nonatomic) IBOutlet UILabel *dataText;

@property (weak, nonatomic) IBOutlet UIButton *myButton;

@property (weak, nonatomic) IBOutlet UIButton *sendQueryButton;

@end
