//
//  HistoryTableViewCell.m
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "HistoryTableViewCell.h"
@import QuartzCore;
@implementation HistoryTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.textOfLocation.adjustsFontSizeToFitWidth=YES;
    self.dateAndTime.adjustsFontSizeToFitWidth=YES;
    self.sendQueryButton.layer.cornerRadius = 8; // this value vary as per your desire
    self.sendQueryButton.clipsToBounds = YES;
    self.imagevw.layer.cornerRadius = 10.0f;
    self.imagevw.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
