//
//  ChatImageViewController.h
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatImageViewController : UIViewController<UIGestureRecognizerDelegate,UIScrollViewDelegate>
@property NSString * imageURL;
@property (weak, nonatomic) IBOutlet UIImageView *chatImage;

@end
